import React, {Component} from 'react'
import {MDBLink} from "mdbreact";

class MenuType extends Component {
    render() {
        switch (localStorage.getItem("role")) {
            case "ЦРС":
                return <>
                    <MDBLink to="/addgroup" className="input_btn green">Добавить группу</MDBLink>
                    <MDBLink to="/addstudent" className="input_btn green">Добавить студента</MDBLink>
                    <MDBLink to="/editgroup" className="input_btn yellow">Изменить группу</MDBLink>
                    <MDBLink to="/deletegroup" className="input_btn red">Удалить группу</MDBLink>
                    <MDBLink to="/editstudent" className="input_btn yellow">Изменить студента</MDBLink>
                    <MDBLink to="/deletestudent" className="input_btn red">Удалить студента</MDBLink>
                </>
            case "РОП":
                return <>
                    <MDBLink to="/addrecord" className="input_btn green">Добавить запись зачетки</MDBLink>
                    <MDBLink to="/editrecord" className="input_btn yellow">Изменить запись зачетки</MDBLink>
                    <MDBLink to="/deleterecord" className="input_btn red">Удалить запись зачетки</MDBLink>
                </>
            case "Управление":
                return <>
                    <MDBLink to="/addrelation" className="input_btn green">Добавить запись связи</MDBLink>
                    <MDBLink to="/editrelation" className="input_btn yellow">Изменить запись связи</MDBLink>
                    <MDBLink to="/deleterelation" className="input_btn red">Удалить запись связи</MDBLink>
                </>
            case "Бухгалтерия":
                return
        }
    }
}

export default MenuType