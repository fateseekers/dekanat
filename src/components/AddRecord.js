import React, {Component} from 'react'
import fetch from 'isomorphic-unfetch'
import Layout from "./Layout";

class AddRecord extends Component{

    constructor(props) {
        super(props);

        this.onChange =this.onChange.bind(this)
        this.addRecord =this.addRecord.bind(this)
    }


    onChange(e){
        this.setState({[e.target.name]: e.target.value})
    }

    addRecord(e){
        e.preventDefault()
        fetch('http://dekanat.std-278.ist.mospolytech.ru/records', {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            body: JSON.stringify(this.state)
        }).then( async (res) => {
            let result = await res.json()
            alert("Зачетка добавлена!")
        })
    }

    render() {
        return (
            <Layout>
                <section className="fullwidthsection">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-center">
                                <h3>ИС "Деканат-Студенты"</h3>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-center">
                                <form className="d-flex flex-column" onSubmit={(e) => this.addRecord(e)}>
                                    <input type="text" name="subject_name" placeholder="Название предмета" onChange={(e) => this.onChange(e)}/>
                                    <input type="text" name="mark" placeholder="Оценка" onChange={(e) => this.onChange(e)}/>
                                    <input type="text" name="semester" placeholder="Семестер" onChange={(e) => this.onChange(e)}/>
                                    <input type="text" name="retake_count" placeholder="Пересдачи" onChange={(e) => this.onChange(e)}/>
                                    <input type="text" name="rec_book_number" placeholder="Номер книжки" onChange={(e) => this.onChange(e)}/>
                                    <button type="submit">Добавить</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </Layout>
        );
    }
}

export default AddRecord