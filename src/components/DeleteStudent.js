import React, {Component} from 'react'
import fetch from 'isomorphic-unfetch'
import Layout from "./Layout";

class DeleteStudent extends Component{

    constructor(props) {
        super(props);

        this.state = {
            isLoaded: false
        }

        this.onChange =this.onChange.bind(this)
        this.deleteStudent =this.deleteStudent.bind(this)
    }


    onChange(e){
        this.setState({[e.target.name]: e.target.value})
    }

    deleteStudent(e){
        e.preventDefault()
        fetch('http://dekanat.std-278.ist.mospolytech.ru/students', {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            body: JSON.stringify(this.state)
        }).then( async (res) => {
            let result = await res.json()
            alert("Студент удален!")
        })
    }

    render() {
        return (
            <Layout>
                <section className="fullwidthsection">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-center">
                                <h3>ИС "Деканат-Студенты"</h3>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-center">
                                <form className="d-flex flex-column" onSubmit={(e) => this.deleteStudent(e)}>
                                    <input type="text" name="id_stud" placeholder="ID студента" onChange={(e) => this.onChange(e)}/>
                                    <button type="submit">Удалить</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </Layout>
        );
    }
}

export default DeleteStudent