import React, {Component} from 'react'
import fetch from 'isomorphic-unfetch'
import Layout from "./Layout";

class GroupList extends Component{

    constructor(props) {
        super(props);

        this.state = {
            isLoaded: false
        }

        this.onChange =this.onChange.bind(this)
        this.searchGroup =this.searchGroup.bind(this)
    }


    onChange(e){
        this.setState({[e.target.name]: e.target.value})
    }

    searchGroup(e){
        e.preventDefault()
        console.log(this.state.group)
        fetch('http://dekanat.std-278.ist.mospolytech.ru/students_by_group', {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            body: JSON.stringify({group: this.state.group})
        }).then( async (res) => {
            let result = await res.json()
            console.log(result)
            this.setState({students: result.students, isLoaded: true})
        })
    }

    render() {
        if(!this.state.isLoaded) {
            return (
                <Layout>
                    <section className="fullwidthsection">
                        <div className="container">
                            <div className="row">
                                <div className="col-12 d-flex flex-column align-items-center">
                                    <h3>ИС "Деканат-Студенты"</h3>
                                    <p>Поиск группы</p>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-12 d-flex flex-column align-items-center">
                                    <form className="d-flex flex-column" onSubmit={(e) => this.searchGroup(e)}>
                                        <input type="text" name="group" placeholder="Номер группы"
                                               onChange={(e) => this.onChange(e)}/>
                                        <button type="submit">Поиск</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </section>
                </Layout>
            );
        } else {
            console.log(this.state.students)
            return (
                <Layout>
                    <section className="fullwidthsection">
                        <div className="container">
                            <div className="row">
                                <div className="col-12 d-flex flex-column align-items-center">
                                    <h3>ИС "Деканат-Студенты"</h3>
                                    <p>Информация о группе</p>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-12 d-flex flex-column">
                                    <p>Номер группы: {this.state.students[0].group_number}</p>
                                    <p>Специальность: {this.state.students[0].train_tendency}</p>
                                    <p>Специализация: {this.state.students[0].specialization}</p>
                                    <p>Форма обучения: {this.state.students[0].educ_form}</p>
                                    <p>Курс: {this.state.students[0].course}</p>
                                    <p>Ученая степень: {this.state.students[0].acad_degree}</p>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-12">
                                    <h3>Список группы</h3>
                                </div>
                            </div>
                            <div className="row">
                                <table className="table">
                                    <tbody>
                                        <tr>
                                            <td>ID студента</td>
                                            <td>ФИО</td>
                                        </tr>
                                        {this.state.students.map((student) => {
                                            return <tr key={student.id_stud}>
                                                <td>{student.id_stud}</td>
                                                <td>{student.stud_name}</td>
                                            </tr>
                                        })}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </section>
                </Layout>
            )
        }
    }
}

export default GroupList