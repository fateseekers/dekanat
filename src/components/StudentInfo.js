import React, {Component} from 'react'
import fetch from 'isomorphic-unfetch'
import Layout from "./Layout";

class GroupList extends Component{

    constructor(props) {
        super(props);

        this.state = {
            isLoaded: false
        }

        this.onChange =this.onChange.bind(this)
        this.searchName =this.searchName.bind(this)
    }


    onChange(e){
        this.setState({[e.target.name]: e.target.value})
    }

    searchName(e){
        e.preventDefault()
        console.log(this.state.name)
        fetch('http://dekanat.std-278.ist.mospolytech.ru/students_by_name', {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            body: JSON.stringify({name: this.state.name})
        }).then( async (res) => {
            let result = await res.json()
            this.setState({students: result.students, isLoaded: true})
        })
    }

    render() {
        if(!this.state.isLoaded) {
            return (
                <Layout>
                    <section className="fullwidthsection">
                        <div className="container">
                            <div className="row">
                                <div className="col-12 d-flex flex-column align-items-center">
                                    <h3>ИС "Деканат-Студенты"</h3>
                                    <p>Поиск студента</p>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-12 d-flex flex-column align-items-center">
                                    <form className="d-flex flex-column" onSubmit={(e) => this.searchName(e)}>
                                        <input type="text" name="name" placeholder="ФИО студента"
                                               onChange={(e) => this.onChange(e)}/>
                                        <button type="submit">Поиск</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </section>
                </Layout>
            );
        } else {
            return (
                <Layout>
                    <section className="fullwidthsection">
                        <div className="container">
                            <div className="row">
                                <div className="col-12 d-flex flex-column align-items-center">
                                    <h3>ИС "Деканат-Студенты"</h3>
                                    <p>Информация о группе</p>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-12 d-flex flex-column">
                                    <p>ФИО: {this.state.students[0].stud_name}</p>
                                    <p>Паспорт: {this.state.students[0].passport}</p>
                                    <p>Адрес: {this.state.students[0].cur_address}</p>
                                    <p>Адрес родителей: {this.state.students[0].parent_address}</p>
                                    <p>Прика о стипендии: {this.state.students[0].scholarship}</p>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-12">
                                    <h3>Отметки</h3>
                                </div>
                            </div>
                            <div className="row">
                                <table className="table">
                                    <tbody>
                                    <tr>
                                        <td>Название предмета</td>
                                        <td>Оценка</td>
                                        <td>Семестр</td>
                                        <td>Пересдачи</td>
                                        <td>Номер книжки</td>
                                    </tr>
                                    {this.state.students.map((student) => {
                                        return <tr key={student.id_stud}>
                                            <td>{student.subject_name}</td>
                                            <td>{student.mark}</td>
                                            <td>{student.semester}</td>
                                            <td>{student.retake_count}</td>
                                            <td>{student.rec_book_number}</td>
                                        </tr>
                                    })}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </section>
                </Layout>
            )
        }
    }
}

export default GroupList