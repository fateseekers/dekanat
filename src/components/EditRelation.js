import React, {Component} from 'react'
import fetch from 'isomorphic-unfetch'
import Layout from "./Layout";

class EditRelation extends Component{

    constructor(props) {
        super(props);

        this.onChange =this.onChange.bind(this)
        this.editRelation =this.editRelation.bind(this)
    }


    onChange(e){
        this.setState({[e.target.name]: e.target.value})
    }

    editRelation(e){
        e.preventDefault()
        fetch('http://dekanat.std-278.ist.mospolytech.ru/relations', {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            body: JSON.stringify(this.state)
        }).then( async (res) => {
            let result = await res.json()
            alert("Связь обновлена!")
        })
    }

    render() {
        return (
            <Layout>
                <section className="fullwidthsection">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-center">
                                <h3>ИС "Деканат-Студенты"</h3>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-center">
                                <form className="d-flex flex-column" onSubmit={(e) => this.editRelation(e)}>
                                    <input type="text" name="id_rel" placeholder="ID связи" onChange={(e) => this.onChange(e)}/>
                                    <input type="text" name="id_stud" placeholder="ID студента" onChange={(e) => this.onChange(e)}/>
                                    <input type="text" name="id_group" placeholder="ID группы" onChange={(e) => this.onChange(e)}/>
                                    <input type="text" name="id_record" placeholder="ID зачетки" onChange={(e) => this.onChange(e)}/>
                                    <button type="submit">Изменить</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </Layout>
        );
    }
}

export default EditRelation