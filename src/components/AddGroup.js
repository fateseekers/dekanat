import React, {Component} from 'react'
import fetch from 'isomorphic-unfetch'
import Layout from "./Layout";

class AddGroup extends Component{

    constructor(props) {
        super(props);

        this.onChange =this.onChange.bind(this)
        this.addGroup =this.addGroup.bind(this)
    }


    onChange(e){
        this.setState({[e.target.name]: e.target.value})
    }

    addGroup(e){
        e.preventDefault()
        fetch('http://dekanat.std-278.ist.mospolytech.ru/group', {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            body: JSON.stringify(this.state)
        }).then( async (res) => {
            let result = await res.json()
            alert("Группа добавлена!")
        })
    }

    render() {
        return (
            <Layout>
                <section className="fullwidthsection">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-center">
                                <h3>ИС "Деканат-Студенты"</h3>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-center">
                                <form className="d-flex flex-column" onSubmit={(e) => this.addGroup(e)}>
                                    <input type="text" name="number" placeholder="Номер группы" onChange={(e) => this.onChange(e)}/>
                                    <input type="text" name="train_tendency" placeholder="Специальность" onChange={(e) => this.onChange(e)}/>
                                    <input type="text" name="specialization" placeholder="Специализация" onChange={(e) => this.onChange(e)}/>
                                    <input type="text" name="educ_form" placeholder="Форма обучения" onChange={(e) => this.onChange(e)}/>
                                    <input type="text" name="course" placeholder="Номер курса" onChange={(e) => this.onChange(e)}/>
                                    <input type="text" name="acad_degree" placeholder="Ученая степень" onChange={(e) => this.onChange(e)}/>
                                    <button type="submit">Добавить</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </Layout>
        );
    }
}

export default AddGroup