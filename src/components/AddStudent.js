import React, {Component} from 'react'
import fetch from 'isomorphic-unfetch'
import Layout from "./Layout";

class AddStudent extends Component{

    constructor(props) {
        super(props);

        this.onChange =this.onChange.bind(this)
        this.addStudent =this.addStudent.bind(this)
    }


    onChange(e){
        this.setState({[e.target.name]: e.target.value})
    }

    addStudent(e){
        e.preventDefault()
        fetch('http://dekanat.std-278.ist.mospolytech.ru/students', {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            body: JSON.stringify(this.state)
        }).then( async (res) => {
            let result = await res.json()
            alert("Студент добавлен!")
        })
    }

    render() {
        return (
            <Layout>
                <section className="fullwidthsection">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-center">
                                <h3>ИС "Деканат-Студенты"</h3>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-center">
                                <form className="d-flex flex-column" onSubmit={(e) => this.addStudent(e)}>
                                    <input type="text" name="stud_name" placeholder="ФИО" onChange={(e) => this.onChange(e)}/>
                                    <input type="text" name="passport" placeholder="Серия номер паспорта" onChange={(e) => this.onChange(e)}/>
                                    <input type="text" name="cur_address" placeholder="Адрес проживания" onChange={(e) => this.onChange(e)}/>
                                    <input type="text" name="parent_address" placeholder="Адрес родителей" onChange={(e) => this.onChange(e)}/>
                                    <input type="text" name="scholarship" placeholder="Приказ о стипендии (если есть)" onChange={(e) => this.onChange(e)}/>
                                    <button type="submit">Добавить</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </Layout>
        );
    }
}

export default AddStudent