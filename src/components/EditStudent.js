import React, {Component} from 'react'
import fetch from 'isomorphic-unfetch'
import Layout from "./Layout";

class EditStudent extends Component{

    constructor(props) {
        super(props);

        this.state = {
            isLoaded: false
        }

        this.onChange =this.onChange.bind(this)
        this.editStudent =this.editStudent.bind(this)
    }


    onChange(e){
        this.setState({[e.target.name]: e.target.value})
    }

    editStudent(e){
        e.preventDefault()
        fetch('http://dekanat.std-278.ist.mospolytech.ru/students', {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            body: JSON.stringify(this.state)
        }).then( async (res) => {
            let result = await res.json()
            alert("Студент обновлен!")
        })
    }

    render() {
        return (
            <Layout>
                <section className="fullwidthsection">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-center">
                                <h3>ИС "Деканат-Студенты"</h3>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-center">
                                <form className="d-flex flex-column" onSubmit={(e) => this.editStudent(e)}>
                                    <input type="text" name="id_stud" placeholder="ID студента" onChange={(e) => this.onChange(e)}/>
                                    <input type="text" name="stud_name" placeholder="ФИО" onChange={(e) => this.onChange(e)}/>
                                    <input type="text" name="passport" placeholder="Серия номер паспорта" onChange={(e) => this.onChange(e)}/>
                                    <input type="text" name="cur_address" placeholder="Адрес проживания" onChange={(e) => this.onChange(e)}/>
                                    <input type="text" name="parent_address" placeholder="Адрес родителей" onChange={(e) => this.onChange(e)}/>
                                    <input type="text" name="scholarship" placeholder="Приказ о стипендии (если есть)" onChange={(e) => this.onChange(e)}/>
                                    <button type="submit">Изменить</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </Layout>
        );
    }
}

export default EditStudent