import React, {Component} from 'react'
import {MDBIcon, MDBLink} from "mdbreact";
import { withRouter } from 'react-router-dom';

class Layout extends Component{
    constructor(props){
        super(props);
        this.goBack = this.goBack.bind(this);
    }

    goBack(){
        this.props.history.goBack()
    }

    render() {
        return(
            <>
                <header className="header position-absolute w-100">
                    <div className="container">
                        <div className="row">
                            <div className="col-12">
                                <div className="d-flex justify-content-between align-items-center">
                                    <MDBIcon size="2x" icon="arrow-left" onClick={this.goBack}/>
                                    <MDBLink className="w-auto logout" to="/logout">
                                        <button>Выход</button>
                                    </MDBLink>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
                <main>
                    {this.props.children}
                </main>
            </>
        )
    }
}

export default withRouter(Layout)
