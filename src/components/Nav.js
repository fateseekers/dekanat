import React, {Component} from 'react'
import {MDBLink} from "mdbreact";
import MenuType from "./MenuType";
import Layout from "./Layout";

class Navigation extends Component{
    render() {
        return (
            <>
                <Layout>
                <section className="fullwidthsection">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 d-flex flex-column align-items-center justify-content-center">
                                <h3 className="text-center">ИС "Деканат-Студенты"</h3>
                                <MDBLink to="/grouplist" className="input_btn blue">Просмотреть список группы</MDBLink>
                                <MDBLink to="/studentinfo" className="input_btn blue">Просмотреть инфо о студенте</MDBLink>
                            </div>
                            <div className="col-12 d-flex flex-column align-items-center mt-4 justify-content-center">
                                <MenuType />
                            </div>
                        </div>
                    </div>
                </section>
                </Layout>
            </>
        );
    }
}

export default Navigation
