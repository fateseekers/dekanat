import React, { Component } from 'react';
import './App.css';
import {MDBAlert, MDBBtn, MDBInput} from "mdbreact";
import fetch from 'isomorphic-unfetch'
import {Redirect} from "react-router-dom";
import {Link} from "react-router-dom";

class App extends Component {
    constructor(props){
        super(props)

        this.state = {
            auth: false,
            login: "",
            password: ""
        }

        this.onSubmit = this.onSubmit.bind(this)
        this.onChange = this.onChange.bind(this)
    }

    onChange(e){
        this.setState({[e.target.name]: e.target.value})
    }

    onSubmit(e){
        e.preventDefault();
        let login = this.state.login,
            password = this.state.password

        if(login === "ЦРС" && password === "ЦРС"){
            this.setState({auth: true})
            localStorage.setItem("role", "ЦРС")
            return
        }
        if(login === "РОП" && password === "РОП"){
            this.setState({auth: true})
            localStorage.setItem("role", "РОП")
            return
        }
        if(login === "Управление" && password === "Управление"){
            this.setState({auth: true})
            localStorage.setItem("role", "Управление")
            return
        }
        if(login === "Бухгалтерия" && password === "Бухгалтерия"){
            this.setState({auth: true})
            localStorage.setItem("role", "Бухгалтерия")
            return
        }
    }



    render() {
        if(!this.state.auth) {
            return (
                <section className="fullwidthsection d-flex align-items-center justify-content-center">
                    <div className="container">
                        <div className="row d-flex justify-content-center">
                            <div className="col-12 col-md-6">
                                <form className="d-flex flex-column align-items-center">
                                    <p className="h5 text-center mb-4">ИС "Деканат-Студенты"</p>
                                    <div className="grey-text d-flex flex-column">
                                        <input className="input" placeholder="Логин" name={"login"}  type="text"
                                                   required
                                                  onChange={(e) => this.onChange(e)}/>
                                        <input className="input" placeholder="Пароль" name={"password"}  type="password"
                                                   required onChange={(e) => this.onChange(e)}/>
                                    </div>
                                    <div className="text-center">
                                        <button className="input_btn" type="submit"
                                                onClick={(e) => this.onSubmit(e)}>Войти</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            );
        } else {
            return <Redirect to="/nav" />
        }
    }
}

export default App;
