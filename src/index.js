import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';

import { HashRouter, Switch, Route } from "react-router-dom";
import Logout from "./components/Logout";
import Navigation from "./components/Nav";
import GroupList from "./components/GroupList";
import StudentInfo from "./components/StudentInfo";
import AddStudent from "./components/AddStudent";
import EditStudent from "./components/EditStudent";
import AddGroup from "./components/AddGroup";
import EditGroup from "./components/EditGroup";
import DeleteGroup from "./components/DeleteGroup";
import DeleteStudent from "./components/DeleteStudent";
import AddRecord from "./components/AddRecord";
import EditRecord from "./components/EditRecord";
import DeleteRecord from "./components/DeleteRecord";
import AddRelation from "./components/AddRelation";
import EditRelation from "./components/EditRelation";
import DeleteRelation from "./components/DeleteRelation";

ReactDOM.render(
    <HashRouter>
        <Switch>
            <Route path="/grouplist" component={GroupList}/>
            <Route path="/studentinfo" component={StudentInfo}/>

            <Route path="/addstudent" component={AddStudent}/>
            <Route path="/editstudent" component={EditStudent}/>
            <Route path="/deletestudent" component={DeleteStudent}/>

            <Route path="/addgroup" component={AddGroup}/>
            <Route path="/editgroup" component={EditGroup}/>
            <Route path="/deletegroup" component={DeleteGroup}/>

            <Route path="/addrecord" component={AddRecord}/>
            <Route path="/editrecord" component={EditRecord}/>
            <Route path="/deleterecord" component={DeleteRecord}/>

            <Route path="/addrelation" component={AddRelation}/>
            <Route path="/editrelation" component={EditRelation}/>
            <Route path="/deleterelation" component={DeleteRelation}/>

            <Route path="/nav" component={Navigation}/>
            <Route path="/logout" component={Logout} />
            <Route exact path="/" component={App} />
        </Switch>
    </HashRouter>
    ,document.getElementById('root')
);
